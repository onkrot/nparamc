﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using MathNet.Numerics.LinearAlgebra;

namespace nparamc
{
    enum OpType
    {
        OneStep,
        TwoStep,
        MaxStep,
        Rec2,
    };
    class ModelProcessor
    {
        /// Корелляция между параметрами (не более)
        private readonly double corelX = 0.7;
        /// Корелляция параметра и y (не менее)
        private readonly double corelY = 0.03;
        private readonly double alp = 0.05;
        private readonly List<Vector<double>> par;
        private Vector<double> Y;
        public ModelProcessor(List<Vector<double>> par, Vector<double> Y, double corelX = 0.7, double corelY = 0.03, double alp = 0.05)
        {
            this.par = par;
            this.Y = Y;
            this.corelX = corelX;
            this.corelY = corelY;
            this.alp = alp;
        }

        private static int BestModelDet(IReadOnlyList<RegressModel> rm)
        {
            int num = 0;
            var t = rm[0].RebuilderDetermination();
            double bestAvg = t.Item1;
            double bestSt = t.Item2;
            for (int i = 1; i < rm.Count; i++)
            {
                t = rm[i].RebuilderDetermination();
                double tAvg = t.Item1;
                double tSt = t.Item2;
                if (tAvg > bestAvg && tSt < bestSt)
                {
                    num = i;
                    bestAvg = tAvg;
                    bestSt = tSt;
                }
            }
            return num;
        }
        private static int BestModelVar(IReadOnlyList<RegressModel> rm)
        {
            int num = 0;
            var t = rm[0].RebuilderDelta();
            double bestAvg = t.Item1;
            double bestSt = t.Item2;
            for (int i = 1; i < rm.Count; i++)
            {
                t = rm[i].RebuilderDelta();
                double tAvg = t.Item1;
                double tSt = t.Item2;
                if (tAvg < bestAvg && tSt < bestSt)
                {
                    num = i;
                    bestAvg = tAvg;
                    bestSt = tSt;
                }
            }
            return num;
        }
        private static int BestModelVarYear(IReadOnlyList<RegressModel> rm)
        {
            int num = 0;
            var t = rm[0].RebuilderDeltaYear();
            double bestAvg = t.Item1;
            double bestSt = t.Item2;
            for (int i = 1; i < rm.Count; i++)
            {
                t = rm[i].RebuilderDelta();
                double tAvg = t.Item1;
                double tSt = t.Item2;
                if (tAvg < bestAvg && tSt < bestSt)
                {
                    num = i;
                    bestAvg = tAvg;
                    bestSt = tSt;
                }
            }
            return num;
        }

        public List<RegressModel> Process(OpType ty, string fileName, double eps)
        {
            StreamWriter sw = new StreamWriter(fileName);
            Params p = new Params();
            for (int i = 0; i < par.Count; i++)
            {
                Block1 bl = new Block1(par[i].Clone(), Y);
                Block1Rec blr = new Block1Rec(Y);
                var t = par[i].Clone();
                Param pt = new Param();
                pt.FirstMax = t.Maximum();
                pt.FirstMin = t.Minimum();
                FuncInfo f = null;
                switch (ty)
                {
                    case OpType.OneStep:
                        f = bl.FindBestOneStep(eps, ref t);
                        break;
                    case OpType.TwoStep:
                        f = bl.FindBestTwoSteps(eps, ref t);
                        break;
                    case OpType.MaxStep:
                        f = bl.FindBestMaxStep(eps, ref t);
                        break;
                    case OpType.Rec2:
                        f = blr.StartRec(par[i].Clone(), 2, out t);
                        break;
                }
                util.Clamp2102(t);
                pt.x = t;
                pt.func = f;
                p.Push(pt);
            }

            ModelGenerator generator = new ModelGenerator(corelX, corelY, alp);
            List<RegressModel> models = generator.GenerateModels(p, Y);

            if (!models.Any())
            {
                return null;
            }

            for (int i = 0; i < models.Count; i++)
            {
                sw.WriteLine($"Model {i}");
                util.WriteModelInfo(models[i], sw);
            }

            List<RegressModel> res = new List<RegressModel>();

            var best1 = BestModelDet(models);
            var best2 = BestModelVar(models);
            var best3 = BestModelVarYear(models);

            res.Add(models[best1]);
            res.Add(models[best2]);
            res.Add(models[best3]);
            sw.WriteLine($"Лучшая по детерминации {best1}");
            util.WriteModelInfo(models[best1], sw);
            sw.WriteLine($"Лучшая по вариации коэффициентов {best2}");
            util.WriteModelInfo(models[best2], sw);
            sw.WriteLine($"Лучшая по вариации коэффициентов (по годам) {best3}");
            util.WriteModelInfo(models[best3], sw);
            sw.Close();
            return res;
        }
    }
}
