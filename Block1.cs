﻿using System;
using System.Collections.Generic;
using System.Linq;
using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Double;
using MathNet.Numerics.Statistics;

namespace nparamc
{
    public class Block1
    {
        private readonly Vector<double> X;

        private readonly Vector<double> Y;

        //private string path;
        private double tc;

        public static readonly Func<double, double>[] funcs =
        {
            x => x * x,
            x => x * x * x,
            x => 1 / x,
            x => 1 / (x * x),
            x => 1 / (x * x * x),
            x => 1 / Math.Cbrt(x),
            Math.Log,
            Math.Sqrt,
            Math.Sin,
            Math.Tan,
            Math.Atan,
        };

        public static readonly string[] func_names =
        {
            "x^2",
            "x^3",
            "1/x",
            "1/x^2",
            "1/x^3",
            "1/cbrt(x)",
            "log",
            "sqrt",
            "sin",
            "tan",
            "atan"
        };

        public static readonly Func<double, double, double>[] eFuncs =
        {
            (x, a) => Math.Exp(x * a),
            (x, a) => Math.Exp(x * x * a),
            (x, a) => 1 / (1 + Math.Exp(x * a)),
            (x, a) => (Math.Exp(x * a) - 1) / (Math.Exp(x * a) + 1),
            (x, a) => (Math.Exp(x * a) + Math.Exp(-x * a)) / 2,
        };

        public static readonly string[] eFunc_names =
        {
            "e^(xa)",
            "e^(xxa)",
            "1/(1+e^(xa))",
            "(e^(xa)-1)/(e^(xa)+1)",
            "(e^(xa)+e^-(xa))/2"
        };

        public static readonly double[] alphas = {
            0.001, -0.001, 0.01, -0.01, 0.1, -0.1, 0.5, -0.5,
            1, -1, 1.5, -1.5, 2, -2, 2.5, -2.5, 3, -3, 3.5, -3.5, 4, -4, 4.5, -4.5, 5, -5,
            6, -6, 7, -7, 8, -8, 9, -9, 10, -10};

        public Block1(Vector<double> x, Vector<double> y)
        {
            X = x;
            Y = y;
            util.Clamp2102(X);
            tc = Correlation.Pearson(X, Y);
        }

        public FuncInfo FindBestOneStep(double eps, ref Vector<double> x)
        {
            Vector<double> newX = DenseVector.Create(x.Count, 0);
            Vector<double> bestX = null;
            util.Clamp2102(x);
            double nc;
            FuncInfo res = new FuncInfo()
            {
                Corel = tc
            };
            Func<double, double> resF = null;
            for (int i = 0; i < funcs.Length; i++)
            {
                var f = funcs[i];
                x.Map(f, newX, Zeros.Include);
                nc = Correlation.Pearson(newX, Y);
                if (Math.Abs(nc) > Math.Abs(res.Corel))
                {
                    if (Math.Abs(nc) - Math.Abs(tc) > eps)
                    {
                        bestX = newX.Clone();
                        resF = f;
                        res.Name = func_names[i];
                        res.Corel = nc;
                    }
                }
            }

            foreach (var alpha in alphas)
            {
                for (int i = 0; i < eFuncs.Length; i++)
                {
                    var f = eFuncs[i];
                    x.Map(t => f(t, alpha), newX, Zeros.Include);
                    nc = Correlation.Pearson(newX, Y);
                    if (Math.Abs(nc) > Math.Abs(res.Corel))
                    {
                        if (Math.Abs(nc) - Math.Abs(tc) > eps)
                        {
                            bestX = newX.Clone();
                            resF = t => f(t, alpha);
                            res.Name = eFunc_names[i] + " a: " + alpha;
                            res.Corel = nc;
                        }
                    }
                }
            }

            if (resF != null)
            {
                x = bestX;
                tc = res.Corel;
                res.FuncList.Add(resF);
                res.MinMax.Add(new Tuple<double, double>(x.Minimum(), x.Maximum()));
            }
            return res;
        }

        public FuncInfo FindBestTwoSteps(double eps, ref Vector<double> x)
        {
            util.Clamp2102(x);
            tc = Correlation.Pearson(x, Y);
            var res = FindBestOneStep(eps, ref x);
            Vector<double> x1 = x.Clone();
            var res2 = FindBestOneStep(eps, ref x1);
            if (res2.FuncList.Any())
            {
                var fi = new FuncInfo()
                {
                    Name = res2.Name + " . " + res.Name
                };
                fi.FuncList.AddRange(res2.FuncList);
                fi.Corel = res2.Corel;
                fi.MinMax.AddRange(res2.MinMax);
                x = x1;
                return fi;
            }

            return res;
        }

        public FuncInfo FindBestMaxStep(double eps, ref Vector<double> x)
        {
            util.Clamp2102(x);
            tc = Correlation.Pearson(x, Y);
            FuncInfo newRes;
            FuncInfo ret = new FuncInfo();
            do
            {
                Vector<double> newX = x.Clone();
                newRes = FindBestOneStep(eps, ref newX);
                if (newRes.FuncList.Any())
                {
                    ret.FuncList.AddRange(newRes.FuncList);
                    ret.MinMax.AddRange(newRes.MinMax);
                    ret.Name = newRes.Name + ret.Name;
                    ret.Corel = newRes.Corel;
                    x = newX;
                }
            } while (newRes.FuncList.Any());

            return ret;
        }
    }

    public class Block1Rec
    {
        private readonly FuncInfo res;
        private readonly Vector<double> Y;
        private Vector<double> bestX;
        private List<Vector<double>> tmp;
        public Block1Rec( Vector<double> y)
        {
            Y = y;
            res = new FuncInfo();
            bestX = DenseVector.Create(y.Count, 0);
        }

        public FuncInfo StartRec(Vector<double> x, int maxDepth, out Vector<double> best)
        {
            tmp = new List<Vector<double>>();
            for (int i = 0; i < maxDepth; i++)
            {
                tmp.Add(DenseVector.Create(x.Count, 0));
            }
            FindBestRec(x, 1, maxDepth, new FuncInfo());
            best = bestX;
            return res;
        }

        private void FindBestRec(Vector<double> x, int depth, int maxDepth, FuncInfo tf)
        {
            Vector<double> newX = tmp[depth - 1];
            util.Clamp2102(x);
            for (int i = 0; i < Block1.funcs.Length; i++)
            {
                var f = Block1.funcs[i];
                x.Map(f, newX, Zeros.Include);
                if (depth < maxDepth)
                {
                    FuncInfo fi = new FuncInfo();
                    fi.FuncList.AddRange(tf.FuncList);
                    fi.Name = tf.Name + " . " + Block1.func_names[i];
                    FindBestRec(newX, depth + 1, maxDepth, fi);
                }
                else
                {
                    double nc = Correlation.Pearson(newX, Y);
                    if (Math.Abs(nc) > Math.Abs(res.Corel))
                    {
                        bestX = newX.Clone();
                        res.FuncList = new List<Func<double, double>>(tf.FuncList)
                        {
                            f
                        };
                        res.Name = tf.Name + "." + Block1.func_names[i]; 
                        res.Corel = nc;
                    }
                }
            }
                

            foreach (var alpha in Block1.alphas)
            {
                for (int i = 0; i < Block1.eFuncs.Length; i++)
                {
                    Func<double, double, double> k = Block1.eFuncs[i];
                    Func<double, double> f = ((t) => k(t, alpha));
                    x.Map(f, newX, Zeros.Include);
                    if (depth < maxDepth)
                    {
                        FuncInfo fi = new FuncInfo();
                        fi.FuncList.AddRange(tf.FuncList);
                        fi.Name = tf.Name + " . " + Block1.eFunc_names[i] + " a: " + alpha;
                        FindBestRec(newX, depth + 1, maxDepth, fi);
                    }
                    else
                    {
                        double nc = Correlation.Pearson(newX, Y);
                        if (Math.Abs(nc) > Math.Abs(res.Corel))
                        {
                            bestX = newX.Clone();
                            res.FuncList = new List<Func<double, double>>(tf.FuncList)
                            {
                                f
                            };
                            res.Name = tf.Name + "." + Block1.eFunc_names[i] + " a: " + alpha;
                            res.Corel = nc;
                        }
                    }
                }
            }
        }
    }

    public class FuncInfo
    {
        public List<Func<double, double>> FuncList;
        public List<Tuple<double, double>> MinMax;
        public string Name;
        public double Corel;
        public FuncInfo()
        {
            Name = "";
            FuncList = new List<Func<double, double>>();
            MinMax = new List<Tuple<double, double>>();
        }
    }
}