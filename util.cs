﻿using System;
using System.Linq;
using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Double;
using MathNet.Numerics;
using MathNet.Numerics.Distributions;
using System.IO;

namespace nparamc
{
    class util
    {
        public const double min = 2;
        public const double max = 7;
        public static double ClampX(double x, double xmin, double xmax)
        {
            return (x - xmin) / (xmax - xmin) * (max - min) + min;
        }

        public static double ClampX2102(double x, double xmin, double xmax) => ClampX(x, xmin, xmax);

        public static void Clamp(Vector<double> x)
        {
            double xmin = x.Minimum();
            double xmax = x.Maximum();
            for (int i = 0; i < x.Count; i++)
            {
                x[i] = (x[i] - xmin) / (xmax - xmin) * (max - min) + min;
            }
        }

        public static void Clamp2102(Vector<double> x) => Clamp(x);

        public static void WriteModelInfo(RegressModel m, TextWriter sw)
        {

            sw.WriteLine(m.GetEquation());
            sw.WriteLine("Адекватность {0}  Нормальность ошибки {1}", m.IsModelAdequate(), m.HasErrorNormalDistribution());
            sw.WriteLine($"Determination {m.Determination(0, 770)}");
            sw.WriteLine();
        }

        // true если коэффициент корреляции статистически значим
        public static bool CheckCorrelation(double r, double n, double m, StudentT d, double alp)
        {
            double tn = (r / Math.Sqrt(1 - r * r)) * Math.Sqrt(n - m - 1);
            double tt = d.InverseCumulativeDistribution(1 - alp);
            return tn > tt;
        }

        // Возврщает значение статистики омега-квадрат
        public static double OmegaSquStat(Vector<double> x)
        {
            Sorting.Sort(x);
            double h_step = 1.0 / x.Count;
            Vector<double> h = DenseVector.Create(x.Count, 0);
            for (int i = 0; i < h.Count; i++)
            {
                h[i] = h_step * i;
            }
            var x_inv = DenseVector.OfVector(x);
            for (int i = 0; i < x_inv.Count; i++)
            {
                x_inv[i] = -x_inv[i];
            }
            var h_inv = DenseVector.Create(h.Count, 0);
            for (int i = 0; i < x_inv.Count; i++)
            {
                if (x_inv[i] < x.First())
                {
                    h_inv[i] = 0;
                    continue;
                }
                if (x_inv[i] > x.Last())
                {
                    h_inv[i] = 1;
                    continue;
                }
                double hp = 0;
                for (int j = 0; j < x.Count; j++)
                {
                    if (x[j] >= x_inv[i])
                    {
                        h_inv[i] = hp;
                        break;
                    }
                    hp = h[j];
                }
            }
            double w = 0;
            for (int i = 0; i < h.Count; i++)
            {
                w += Math.Pow(h[i] + h_inv[i] - 1, 2);
            }
            return w;
        }
        public static void ReadDataFromFile(string file, ref Matrix<double> X, ref Vector<double> Y)
        {
            int n, m;
            double[,] x;
            double[] y;
            using (StreamReader sr = new StreamReader("data.txt"))
            {
                string s = sr.ReadLine();
                string[] sd = s.Split();
                n = int.Parse(sd[0]);
                m = int.Parse(sd[1]);
                sd = sr.ReadLine().Split();
                y = new double[n];
                for (int i = 0; i < sd.Length; i++)
                {
                    y[i] = double.Parse(sd[i]);
                }
                x = new double[n, m + 1];
                for (int i = 0; i < n; i++)
                {
                    x[i, 0] = 1;
                }

                for (int i = 1; i < m + 1; i++)
                {
                    sd = sr.ReadLine().Split();
                    for (int j = 0; j < n; j++)
                    {
                        x[j, i] = double.Parse(sd[j]);

                    }
                }
            }

            X = DenseMatrix.OfArray(x);
            Y = CreateVector.DenseOfArray(y);
        }
    }
}
