﻿using MathNet.Numerics.Distributions;
using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Double;
using MathNet.Numerics.Statistics;
using System;
using System.Collections.Generic;
using System.Linq;

namespace nparamc
{
    class ModelGenerator
    {
        private readonly List<List<int>> mod = new List<List<int>>();
        private readonly List<int> tm = new List<int>();
        private readonly double corelX;
        private readonly double corelY;
        private readonly double alp;
        private int m;
        private int n;
        public ModelGenerator(double corelX, double corelY, double alp)
        {
            this.corelX = corelX;
            this.corelY = corelY;
            this.alp = alp;  
        }
        public List<RegressModel> GenerateModels(Params par, Vector<double> Y)
        {
            m = par.X.Count;
            n = Y.Count;

            // Выбираем только те параметры, для которых коэффициент корелляции с Y значим
            StudentT student = new StudentT(0, 1, n - 2);

            Params good = new Params();
            Param one = new Param()
            {
                FirstMax = 1,
                FirstMin = 1,
                func = new FuncInfo(),
                x = DenseVector.Create(770, 1)
            };
            for (int i = 0; i < m; i++)
            {
                double c = Math.Abs(par.pars[i].func.Corel);
                if (util.CheckCorrelation(c, n, m, student, alp) && c > corelY)
                    good.Push(par.pars[i]);
                    
            }
            m = good.X.Count;

            // Из них формируем группы сильно кореллированых между собой
            List<List<int>> modelsCol = new List<List<int>>();
            for (int i = 1; i < m; i++)
            {
                bool found = false;
                int foundI = 0;
                for (int j = 0; j < modelsCol.Count; j++)
                {
                    for (int k = 0; k < modelsCol[j].Count; k++)
                    {
                        if (Correlation.Pearson(good.X[i], good.X[modelsCol[j][k]]) > corelX)
                        {
                            found = true;
                            foundI = j;
                            goto exit;
                        }
                    }
                }
                exit:
                if (found)
                {
                    modelsCol[foundI].Add(i);
                }
                else
                {
                    modelsCol.Add(new List<int>());
                    modelsCol.Last().Add(i);
                }
            }

            // Преобразуем список групп к списку параметров моделей
            Helper(modelsCol, 0);
            var models = mod;

            // Формируем модели
            List<RegressModel> rm = new List<RegressModel>();
            foreach (var it in models)
            {
                Params u = new Params();
                u.Push(one);
                foreach (var ip in it)
                {
                    u.Push(good.pars[ip]);
                }
                var rc = new RegressModel(u, Y, alp, it);
                if (rc.HasErrorNormalDistribution())
                {
                    rm.Add(rc);
                }
                
            }
            return rm;
        }

        // Рекурсивная функция формирования списков параметров
        private void Helper(IReadOnlyList<List<int>> mC, int depth)
        {
            if (depth == mC.Count)
            {
                mod.Add(tm.ToList());
                return;
            }
            for (int i = 0; i < mC[depth].Count; i++)
            {
                tm.Add(mC[depth][i]);
                Helper(mC, depth + 1);
                tm.RemoveAt(tm.Count - 1);
            }
        }
    }
}
