﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Double;
using MathNet.Numerics.Statistics;

namespace nparamc
{
    struct ExpandedVal
    {
        public double Min;
        public double Max;
        public double Sigma;
    }
    class Block2
    {
        private readonly bool[][] Relations;
        public readonly List<RegressModel> Models;
        public List<ExpandedVal> Expanded;
        private readonly List<int> num;
        public Block2(bool[][] relations, IReadOnlyList<Vector<double>> par, double expV, List<int> nums) 
        {
            Expanded = new List<ExpandedVal>();
            int n = par.Count;
            num = nums;
            foreach (var p in par)
            {
                double min = p.Minimum();
                double max = p.Maximum();
                double avg = p.Average();
                double minp = (avg - min) / (max - min);
                double maxp = (max - avg) / (max - min);
                double t = ((max - min) / expV);
                ExpandedVal e = new ExpandedVal
                {
                    Min = min - expV * maxp * t,
                    Max = max + expV * minp * t,
                    Sigma = p.StandardDeviation()
                };
                Expanded.Add(e);
            }
            Relations = relations;

            Models = new List<RegressModel>
            {
                null
            };
            
            var one = new Param
            {
                x = DenseVector.Create(770, 1)
            };
            
            for (int i = 1; i < n; i++)
            {
                var pars = new Params();
                pars.Push(one);
                for (int j = 0; j < i; j++)
                {
                    if (!relations[num[i]][num[j]]) continue;
                    var p = new Param 
                    {
                        x = par[j]
                    };
                    pars.Push(p);
                }
                Models.Add(new RegressModel(pars, par[i], 0.05, new List<int>()));
            }
        }

        public double Eval(List<double> newX, int num_i)
        {
            List<double> x = new List<double>
            {
                1.0
            };
            for (int i = 0; i < newX.Count; i++)
            {
                if(Relations[num[num_i]][num[i]]) x.Add(newX[i]);
            }
            return Models[num_i].Eval(DenseVector.OfEnumerable(x));
        }
    }
}
