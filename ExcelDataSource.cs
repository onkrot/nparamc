﻿using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Double;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System.Collections.Generic;
using System.IO;

namespace nparamc
{
    class ExcelDataSource
    {
        readonly IWorkbook wb;

        public ExcelDataSource(string file)
        {
            var fs = new FileStream(file, FileMode.Open, FileAccess.Read);
            wb = new HSSFWorkbook(fs);
        }

        private Vector<double> GetCol(int row, int start, int count, ISheet ws)
        {
            var v = DenseVector.Create(count, 0);
            for (int i = 0; i < count; i++)
            {
                v[i] = ws.GetRow(i + start).GetCell(row, MissingCellPolicy.RETURN_NULL_AND_BLANK).NumericCellValue;
            }
            return v;
        }
        public void ExtractData(int m,int variant, out List<Vector<double>> X, out Vector<double> Y, out bool[][] relations)
        {
            var ws = wb.GetSheetAt(1);
            int[] priority = new int[m];
            for (int i = 0; i < m; i++)
            {
                priority[i] = (int)ws.GetRow(i + 1).GetCell(m + 1, MissingCellPolicy.RETURN_NULL_AND_BLANK).NumericCellValue - 1;
            }
            relations = new bool[m][];
            for (int i = 0; i < m; i++)
            {
                relations[priority[i]] = new bool[m];
                for (int j = 0; j < m; j++)
                {
                    relations[priority[i]][priority[j]] = ws.GetRow(i + 1).GetCell(j + 1, MissingCellPolicy.RETURN_NULL_AND_BLANK).NumericCellValue == 1;
                }
            }
            ws = wb.GetSheetAt(0);
            X = new List<Vector<double>>();
            for (int i = 0; i < m; i++)
            {
                X.Add(new DenseVector(1));
            }
            for (int i = 0; i < m; i++)
            {
                X[priority[i]] = GetCol(i + 2, 1, 770, ws);
            }
            Y = GetCol(m + 1 + variant, 1, 770, ws);
        }
    }
}
