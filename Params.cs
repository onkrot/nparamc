﻿using System;
using System.Collections.Generic;
using System.Text;
using MathNet.Numerics.LinearAlgebra;

namespace nparamc
{
    class Param
    {
        public Vector<double> x;
        public FuncInfo func;
        public double FirstMin;
        public double FirstMax;
    }
    class Params
    {
        public List<Vector<double>> X { get; private set; }
        public List<FuncInfo> func { get; private set; }
        public readonly List<Param> pars;
        private void Push(Vector<double> x, FuncInfo f)
        {
            X.Add(x);
            func.Add(f);
        }

        public void Push(Param p)
        {
            pars.Add(p);
            Push(p.x, p.func);
        }

        public Params()
        {
            X = new List<Vector<double>>();
            func = new List<FuncInfo>();
            pars = new List<Param>();
        }
    }
}
