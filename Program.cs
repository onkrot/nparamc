﻿using MathNet.Numerics.LinearAlgebra;
using System;
using System.Collections.Generic;
using System.IO;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using MathNet.Numerics.LinearAlgebra.Double;

namespace nparamc
{
    class Program
    {
        private static void WriteModels(IReadOnlyList<RegressModel> models, TextWriter sw, int[] k)
        {
            if (models == null)
            {
                return;
            }
            sw.WriteLine($"{k[0]} Лучшая по детерминации");
            util.WriteModelInfo(models[0], sw);
            sw.WriteLine($"{k[1]} Лучшая по вариации коэффициентов");
            util.WriteModelInfo(models[1], sw);
            sw.WriteLine($"{k[2]} Лучшая по вариации коэффициентов (по годам)");
            util.WriteModelInfo(models[2], sw);
        }
        static void Main(string[] args)
        {

            const int m = 17;
            const double eps = 0.001;
            ExcelDataSource source = new ExcelDataSource(@"source.xls");
            source.ExtractData(m, 1, out List<Vector<double>> par, out Vector<double> Y, out bool[][] relations);

            var stopwatch = new Stopwatch();
            stopwatch.Start();
            ModelProcessor processor = new ModelProcessor(par, Y);
            var oneStepModels = processor.Process(OpType.OneStep, "info_one.txt", eps);
            var twoStepModels = processor.Process(OpType.TwoStep, "info_two.txt", eps);
            var maxStepModels = processor.Process(OpType.MaxStep, "info_max.txt", eps);
            var rec2Models = processor.Process(OpType.Rec2, "info_rec.txt", eps);

            stopwatch.Stop();
            long elapsedTime = stopwatch.ElapsedMilliseconds;
            Console.WriteLine($"total time: {elapsedTime} ms");

            Console.WriteLine("Лучшие одношаговые модели");
            WriteModels(oneStepModels, Console.Out, new[] { 1, 2, 3 });
            Console.WriteLine("Лучшие модели на 2 шага");
            WriteModels(twoStepModels, Console.Out, new[] { 4, 5, 6 });
            Console.WriteLine("Лучшие модели на макскимальное число шагов");
            WriteModels(maxStepModels, Console.Out, new[] { 7, 8, 9 });
            Console.WriteLine("Лучшие модели на полный перебор");
            WriteModels(rec2Models, Console.Out, new[] { 10, 11, 12 });
            List<RegressModel> allModels = new List<RegressModel>();
            allModels.AddRange(oneStepModels);
            allModels.AddRange(twoStepModels);
            allModels.AddRange(maxStepModels);
            allModels.AddRange(rec2Models);
            int cm = 7;
            while (true)
            {
                Console.Write("Выберите нужную модель (1-12, 7): ");
                string s = Console.ReadLine();
                if (s == "") break;
                int y;
                if (int.TryParse(s, out y))
                {
                    if (y >= 1 && y <= 12)
                    {
                        cm = y;
                        break;
                    }
                }
            }

            var modelChosen = allModels[cm - 1];

            Console.WriteLine("Управление");
            Console.WriteLine("Процент расширения");
            double p = double.Parse(Console.ReadLine());

            List<int> num = modelChosen.num;
            List<Vector<double>> block2List = new List<Vector<double>>();
            foreach (int i in num)
            {
                block2List.Add(par[i]);
            }
            Block2 block2 = new Block2(relations, block2List, p, num);
            while (true)
            {
                List<double> newX = new List<double>
                {
                    1.0
                };
                double k = 1;
                for (int i = 0; i < num.Count; i++)
                {

                    double next_x;
                    double min;
                    double max;
                    if (i > 0)
                    {
                        next_x = block2.Eval(newX, i);
                        min = Math.Max(next_x - k * block2.Expanded[i].Sigma, 0);
                        max = next_x + k * block2.Expanded[i].Sigma;
                        k -= 0.05;
                    }
                    else
                    {
                        min = Math.Max(block2.Expanded[i].Min, 0);
                        max = block2.Expanded[i].Max;
                        next_x = (min + max) / 2;
                    }
                    Console.WriteLine($"Введите x{num[i] + 1} или оставить ({next_x}) (min: {min} max: {max}): ");
                    string s = Console.ReadLine();
                    double x;
                    if (!string.IsNullOrWhiteSpace(s))
                    {
                        x = double.Parse(s);
                        if (x < min || x > max)
                        {
                            Console.WriteLine("Внимание! Введённый x лежит вне допустимых пределов. Дальнейше вычисления могут дать неадекваный результат.");
                        }
                    }
                    else
                    {
                        x = next_x;
                    }

                    newX.Add(x);

                }
                Console.WriteLine($"Вычисленное значение {modelChosen.Eval(DenseVector.OfEnumerable(newX))}");
                Console.WriteLine("Продолжить? (Y/n):");
                string a = Console.ReadLine();
                if(a == "n")
                {
                    break;
                }
            }
            
        }
    }
}
